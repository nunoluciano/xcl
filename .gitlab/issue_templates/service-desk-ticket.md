## Summary

(Summarize the bug encountered concisely)

## Steps to reproduce

(How one can reproduce the issue - this is very important)

## Example

(If possible, please add an example code or link that  
exhibits the problematic behavior, here in the bug report.  
If you are using an older version of XOOPSCUBE,  
this will also determine whether the bug has been fixed
in a more recent version)

## What is the current bug behavior?

(What actually happens)

## What is the expected correct behavior?

(What you should see instead)

## Relevant logs and/or screenshots

(Paste any relevant logs - please use code blocks (```) to format console output,  
1logs, and code, as it's very hard to read otherwise.)

## Possible fixes

(If you can, link to the line of code that might be responsible for the problem)


/label ~bug ~service-desk-ticket
/assign @nunoluciano
