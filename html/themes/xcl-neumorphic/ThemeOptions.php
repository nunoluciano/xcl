<?php
/**
 * XCL Theme Neumorphic Design
 * @package    XCL231
 * @author     Original Author : Nuno Luciano (aka Gigamaster)
 * @copyright  2005-2022 The XOOPSCube Project
 * @license    Legacy : https://github.com/xoopscube/xcl/blob/master/GPL_V2.txt
 * @license    Cube : https://github.com/xoopscube/xcl/blob/master/BSD_license.txt
 * @version    Release: @package_231@
 * @link       https://github.com/xoopscube/xcl
*/
$REFERER = $_SERVER['HTTP_REFERER'];
if(!preg_match("@^http:\/\/(www\.)?$domain\/@",$REFERER)){
    die("This page can't be call directly");
}
echo 'Theme Options</h1>
    <h4>Not available</h4>';
