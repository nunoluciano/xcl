<?php
/**

[Manifesto]
Name="XCL Neumorphic"
Depends=Legacy_RenderSystem,legacy
ThemeOptions="index.php"
Url="https://github.com/xoopscube/xcl"
Version="2.3.1"

[Theme]
RenderSystem=Legacy_RenderSystem
Format="XOOPS2 Legacy Style"
Author=XOOPSCube Project Team
ScreenShot="screenshot.png"
Description="Neumorphism design built with xLayout Flexbox, jQuery UI and CSS Custom properties."
W3C=NG

License="https://creativecommons.org/licenses/by/4.0/"

*/

