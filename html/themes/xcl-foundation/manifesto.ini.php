<?php
/**

[Manifesto]
Name="XCL-Foundation"
Depends=Legacy_RenderSystem,legacy
Url="https://github.com/xoopscube/xcl"
Version="1.0.0"

[Theme]
RenderSystem=Legacy_RenderSystem
Format="XOOPS2 Legacy Style"
Author=Nuno Luciano aka gigamaster
ScreenShot="xcl-bootstrap-default.png"
Description="XCL Foundation 6.6.3 built with default layouts and modules templates."
W3C=NG

Licence="MIT"

*/
