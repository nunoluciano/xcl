<?php

// _MI_<MODULENAME>_<STRINGNAME>

// compatibility
Define( '_MI_XOONIPS_INSTALL_DOWNLOAD_LIMIT_LOGIN_USER' , 'Login User');
Define( '_MI_XOONIPS_INSTALL_DOWNLOAD_LIMIT_EVERYONE' , 'Everyone');
Define( '_MI_XOONIPS_INSTALL_DOWNLOAD_NOTIFY_YES' , 'Yes');
Define( '_MI_XOONIPS_INSTALL_DOWNLOAD_NOTIFY_NO' , 'No');

// The name of this module
Define( '_MI_XOONIPS_NAME' , 'XooNIps');
// A brief description of this module
Define( '_MI_XOONIPS_DESC' , 'XooNIps Module');

//userlist block label
Define( '_MI_XOONIPS_ITEMTYPE_BNAME1' , 'User List');
Define( '_MI_XOONIPS_ITEM_BNAME1' , 'Search');
Define( '_MI_XOONIPS_INDEX_BNAME1' , 'Index Tree');

// Names of admin menu items
Define( '_MI_XOONIPS_ACCOUNT_BNAME1' , 'Login');
Define( '_MI_XOONIPS_ACCOUNT_BNAME2' , 'User Menu');

//administrator menu
Define( '_MI_XOONIPS_ADMENU1' , 'System Configuration');
Define( '_MI_XOONIPS_ADMENU2' , 'Site Policies');
Define( '_MI_XOONIPS_ADMENU3' , 'Maintenance');

//notification
Define( '_MI_XOONIPS_USER_NOTIFY' , 'User');
Define( '_MI_XOONIPS_USER_NOTIFYDSC' , 'Notifications for users.');

Define( '_MI_XOONIPS_ADMINISTRATOR_NOTIFY' , 'Administrator');
Define( '_MI_XOONIPS_ADMINISTRATOR_NOTIFYDSC' , 'Notifications for Moderator and Group administrator.');

Define( '_MI_XOONIPS_COMMON_NOTIFY' , 'Common');
Define( '_MI_XOONIPS_COMMON_NOTIFYDSC' , 'Common notification');

// use XooNIpsNotification. subject are defined in main.php
Define( '_MI_XOONIPS_ACCOUNT_CERTIFY_NOTIFY' , 'Account notification');
Define( '_MI_XOONIPS_ACCOUNT_CERTIFY_NOTIFYCAP' , 'Account notification');
Define( '_MI_XOONIPS_ACCOUNT_CERTIFY_NOTIFYDSC' , 'Receive notification when you are requested to approval the account.');

Define( '_MI_XOONIPS_USER_ITEM_TRANSFER_NOTIFY' , 'Notify me of the owner of item changed');
Define( '_MI_XOONIPS_USER_ITEM_TRANSFER_NOTIFYCAP' , 'Notify me of the owner of item changed');
Define( '_MI_XOONIPS_USER_ITEM_TRANSFER_NOTIFYDSC' , 'Receive notification when you are requested to inherit items or your request to transfer items is accepted/rejected');

Define( '_MI_XOONIPS_USER_ITEM_UPDATED_NOTIFY' , 'Item updated');
Define( '_MI_XOONIPS_USER_ITEM_UPDATED_NOTIFYCAP' , 'Item updated');
Define( '_MI_XOONIPS_USER_ITEM_UPDATED_NOTIFYDSC' , 'Receive notification when items updated');

Define( '_MI_XOONIPS_USER_FILE_DOWNLOADED_NOTIFY' , 'File downloaded');
Define( '_MI_XOONIPS_USER_FILE_DOWNLOADED_NOTIFYCAP' , 'File downloaded');
Define( '_MI_XOONIPS_USER_FILE_DOWNLOADED_NOTIFYDSC' , 'Receive notification when a file is downloaded.');

Define( '_MI_XOONIPS_COMMON_GROUP_NOTIFY' , 'Group notification');
Define( '_MI_XOONIPS_COMMON_GROUP_NOTIFYCAP' , 'Group notification');
Define( '_MI_XOONIPS_COMMON_GROUP_NOTIFYDSC' , 'Receive notification when have some changes about group.');

Define( '_MI_XOONIPS_COMMON_ITEM_NOTIFY' , 'Item notification');
Define( '_MI_XOONIPS_COMMON_ITEM_NOTIFYCAP' , 'Item notification');
Define( '_MI_XOONIPS_COMMON_ITEM_NOTIFYDSC' , 'Receive notification when have some changes about item.');

// sub menu
Define( '_MI_XOONIPS_USER_REGISTER_ITEM' , 'New Item Register');
Define( '_MI_XOONIPS_USER_SEARCH' , 'Search');
Define( '_MI_XOONIPS_USER_EDIT_INDEX' , 'Index Edit');
Define( '_MI_XOONIPS_USER_IMPORT_ITEM' , 'Item Import');

//Preference
//removed XooNIps expand User Module
Define( '_MI_XOONIPS_CONF_CERTIFY_USER' , 'Method of approval of new registered user account');
Define( '_MI_XOONIPS_CONF_CER_USER_DESC' , 'It is necessary to approve the user account so that the user who makes the account effective may use XooNIps. This account approval method is set here.');
Define( '_MI_XOONIPS_CONF_USER_CERTIFY_DATE' , 'Approval waiting period of account(day)');
Define( '_MI_XOONIPS_CONF_LOGIN_AUTH_METHOD' , 'Login attestation setting');
Define( '_MI_XOONIPS_CONF_CERTIFY_USER_AUTO' , 'The account is automatically approved');
Define( '_MI_XOONIPS_CONF_CERTIFY_USER_BY_MODERATOR' , 'The moderator confirms and the account is approved');
Define( '_MI_XOONIPS_CONF_XOONIPS_LABEL' , 'XooNIps attestation');
Define( '_MI_XOONIPS_CONF_GROUP_CONSTRUCT_PERMIT' , 'Allow group creation');
Define( '_MI_XOONIPS_CONF_GROUP_CONSTRUCT_ACCEPT' , 'The approval method of creating group');
Define( '_MI_XOONIPS_CONF_GROUP_PUBLIC_ACCEPT' , 'The approval method of Publicing group');
Define( '_MI_XOONIPS_CONF_NOT_PERMIT' , 'Not permit');
Define( '_MI_XOONIPS_CONF_PERMIT' , 'Permit');
Define( '_MI_XOONIPS_CONF_AUTO_ADMIT' , 'Auto-approve user-created groups');
Define( '_MI_XOONIPS_CONF_MODERATOR_ADMIT' , 'Moderator-approve user-created groups');
Define( '_MI_XOONIPS_CONF_AUTO_PUBLIC_ADMIT' , 'Auto-approve to publish the group');
Define( '_MI_XOONIPS_CONF_MODERATOR_PUBLIC_ADMIT' , 'Moderator-approve to publish the group');

// ranking
Define( '_MI_XOONIPS_RANKING' , 'XooNIps Ranking');
