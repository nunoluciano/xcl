<?php

define( '_MD_A_MYMENU_MYBLOCKSADMIN' , 'Blocks Management');

//define( '_AM_MENU_MYTPLSADMIN' , 'Templates Management');
//define( '_AM_MENU_MYBLOCKSADMIN' , 'Blocks Management');
//define( '_AM_MENU_MYLANGADMIN' , 'Language Constant Management');
//define( '_AM_MENU_MYPREFERENCES' , 'Preferences');

define( '_AM_XSNS_POST_DATE' , 'Posted Date');

define( '_AM_XSNS_PERM_ERR' , 'Please create the following directory.<br>* Please set the permission of each directory to 777 when you use the UNIX/LINUX server.');

define( '_AM_XSNS_NOTICE_MYPAGE' , '<div class="error">PHP Safe Mode. PHP executes scripts in a restricted environment. The replacement process of My Page could not be executed.</div>');

define( '_AM_XSNS_NOTICE_FILE_PATH' , 'Configuration for images and files uploads is set in public directory.<br>Due to security considerations, the directory to which files are uploaded should be outside of the public html directory.');

define( '_AM_XSNS_GD_ERR_NONE' , '<div class="error">GD Library extension not available with this PHP installation. The uploading function for images cannot be used.</div>');
define( '_AM_XSNS_GD_ERR_GIF' , 'The GIF image is not supported. ');
define( '_AM_XSNS_GD_ERR_JPG' , 'The JPEG image is not supported. ');
define( '_AM_XSNS_GD_ERR_PNG' , 'The PNG image is not supported. ');

define( '_AM_XSNS_TITLE_CATEGORY_CONFIG' , 'Community Groups');
define( '_AM_XSNS_TITLE_IMAGE_CONFIG' , 'Image Manager');
define( '_AM_XSNS_TITLE_FILE_CONFIG' , 'File Manager');
define( '_AM_XSNS_TITLE_ACCESS_LOG' , 'Access logs');

define( '_AM_XSNS_CATEGORY1' , 'Main Categories ');
define( '_AM_XSNS_CATEGORY2' , 'Subcategories ');
define( '_AM_XSNS_CATEGORY_NAME' , 'Category Name');
define( '_AM_XSNS_CATEGORY_ORDER' , 'Display Order');
define( '_AM_XSNS_CATEGORY_LIST' , 'View');
define( '_AM_XSNS_CATEGORY_OPERATION' , 'Action');
define( '_AM_XSNS_CATEGORY_EDIT' , 'Edit');
define( '_AM_XSNS_CATEGORY_DEL' , 'Delete');
define( '_AM_XSNS_CATEGORY_ADD' , 'Add');
define( '_AM_XSNS_CATEGORY_NAME_NG' , 'Please input the category name. ');
define( '_AM_XSNS_CATEGORY_ORDER_NG' , 'Please input the order of the display.');
define( '_AM_XSNS_CATEGORY_ADD_OK' , 'The category has been added. ');
define( '_AM_XSNS_CATEGORY_ADD_NG' , 'Error: It failed in the addition of the category.');
define( '_AM_XSNS_CATEGORY_EDIT_OK' , 'The category was changed. ');
define( '_AM_XSNS_CATEGORY_EDIT_NG' , 'Error: It failed in the change of the category.');
define( '_AM_XSNS_CATEGORY_DEL_OK' , 'The category was deleted.');
define( '_AM_XSNS_CATEGORY_DEL_NG' , 'Error: It failed in the deletion of the category.');
define( '_AM_XSNS_CATEGORY_DEL_CONFIRM' , 'When the main category is deleted, all of its subcategories are deleted. \\n\\nConfirm delete?');

define( '_AM_XSNS_IMAGE' , 'Image');
define( '_AM_XSNS_IMAGE_SIZE' , 'Size');
define( '_AM_XSNS_IMAGE_AUTHOR' , 'Author');
define( '_AM_XSNS_IMAGE_REF' , 'Reference');
define( '_AM_XSNS_IMAGE_DELETE' , 'Delete');
define( '_AM_XSNS_IMAGE_DELETE_OK' , 'The file which is selected was deleted.');
define( '_AM_XSNS_IMAGE_SELECT_NG' , 'The image is not selected.');
define( '_AM_XSNS_IMAGE_DELETE_NG' , 'The image could not be deleted.');

define( '_AM_XSNS_FILE_NAME' , 'File Name');
define( '_AM_XSNS_FILE_SIZE' , 'Size');
define( '_AM_XSNS_FILE_AUTHOR' , 'Author');
define( '_AM_XSNS_FILE_REF' , 'Reference');
define( '_AM_XSNS_FILE_DELETE' , 'Delete');
define( '_AM_XSNS_FILE_DELETE_OK' , 'The file which is selected was deleted.');
define( '_AM_XSNS_FILE_SELECT_NG' , 'The file is not selected.');
define( '_AM_XSNS_FILE_DELETE_NG' , 'The file could not be deleted.');

define( '_AM_XSNS_ACCESS_USER' , 'User Name');
define( '_AM_XSNS_ACCESS_COMMU' , 'Group Name');
define( '_AM_XSNS_ACCESS_DATE' , 'Access Date');

define( '_AM_XSNS_PAGE_SELECT_DESC' , 'Listing %d-%d of %d.');
