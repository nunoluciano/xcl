<?php
// Syntax replace define with Define( 'v2.3.0 2021/05/15 @gigamaster XCL-PHP7
//          index.php          //
Define( '_MD_PRINTER' , 'PDF に印刷';
Define( '_MD_SENDSTORY' , 'このニュースを友達に送る';
Define( '_MD_READMORE' , '続きを読む';
Define( '_MD_COMMENTS' , '0コメント';
Define( '_MD_ONECOMMENT' , '1コメント';
Define( '_MD_BYTESMORE' , '残り%s字';
Define( '_MD_NUMCOMMENTS' , '%sコメント';

//          submit.php          //
Define( '_MD_SUBMITNEWS' , 'ニュース投稿';
Define( '_MD_TITLE' , '表題';
Define( '_MD_TOP' , 'トップ';
Define( '_MD_TOPIC' , 'カテゴリ';
Define( '_MD_THESCOOP' , 'メッセージ本文';
Define( '_MD_INPUTHELPER' , '入力支援機能のON/OFF';
Define( '_MD_FCKXOOPS_ONOFF' , 'FCK EditorのON/OFF（エディタを切り替える際に編集した内容は保存されません）';
Define( '_MD_ERROR_REQUIRED' , '{0}は必ず入力して下さい';
Define( '_MD_NOTIFYPUBLISH' , 'ニュースが承認された旨をメールで受け取る';
Define( '_MD_POST' , '投稿する';
Define( '_MD_GO' , '送信';
Define( '_MD_THANKS' , '投稿を受付けました。当サイトスタッフによる承認を経た後に正式掲載となることをご了承ください。'; //submission of news article
Define( '_MD_THANKS_AUTOAPPROVE' , '投稿ありがとうございました。';
Define( '_MD_THANKS_BUT_ERROR' , '投稿ありがとうございました。しかし、<span style="color:red">エラー</span>が発生したため記事が送信されませんでした。お手数ですが管理者にお問い合わせください。';
Define( '_MD_USE_HTML' , 'HTMLを有効にする';
Define( '_MD_USE_SMILEY' , '顔アイコンを有効にする';
Define( '_MD_USE_BR' , '改行を自動挿入する';
Define( '_MD_USE_XCODE' , 'XOOPSコードを有効にする';
Define( '_MD_NO_TOPICS' , 'カテゴリがありません。';
Define( '_MD_TOPIC_IMAGE' , 'カテゴリアイコン'; // ver 2.00 added
Define( '_MD_TOPIC_DISABLE' , 'カテゴリアイコンを表示しない'; // ver 2.00 added
Define( '_MD_TOPIC_LEFT' , 'カテゴリアイコンを左側に表示する'; // ver 2.00 added
Define( '_MD_TOPIC_RIGHT' , 'カテゴリアイコンを右側に表示する'; // ver 2.00 added
Define( '_MD_PUBINHOME' , 'メインページに掲載する'; // ver 2.00 added
Define( '_MD_PUBLISHED' , '掲載開始日時'; // ver 2.00 added
Define( '_MD_EXPIRED' , '掲載終了日時'; // ver 2.00 added
Define( '_MD_SETDATETIME' , '掲載日時を設定する'; // ver 2.00 added
Define( '_MD_SETDATETIME_DESC' , '*チェックをしないと掲載日時は現在の日時になります。'; // ver 2.00 added
Define( '_MD_SETEXPDATETIME' , '掲載期限を設定する'; // ver 2.00 added
Define( '_MD_SETEXPDATETIME_DESC' , '*チェックをしないと掲載期限の設定は無効になります。'; // ver 2.00 added
Define( '_MD_APPROVE' , 'この記事を掲載承認状態にする'; // ver 2.00 added
Define( '_MD_DATE_FORMAT' , '%y 年 %m 月 %d 日 %h 時 %i 分 %s 秒'; // ver 2.00 added
Define( '_MD_DBPUDATED' , 'データベースの更新に成功しました。'; // ver 2.00 added
Define( '_MD_POSTEDBY' , '投稿者'; // ver 2.00 added
Define( '_MD_RELATION' , 'この記事に関連した記事'; // ver 2.00 added
Define( '_MD_ADD_RELATION' , '関連記事を追加'; // ver 2.00 added
Define( '_MD_CHECKED_AS_RELATION' , '関連記事に追加'; // ver 2.00 added
Define( '_MD_DISP_BLOCK' , 'ブロックに掲載する'; // ver 2.00 added
Define( '_MD_RUSUREDEL' , 'このニュース記事および記事に対するコメントを全て削除してもいいですか？'; // ver 2.00 added
Define( '_MD_EMPTYNODELETE' , '削除できません'; // ver 2.00 added
//          archive.php          //
Define( '_MD_NEWSARCHIVES' , 'ニュースアーカイブ';
Define( '_MD_ARTICLES' , 'ニュース';
Define( '_MD_VIEWS' , 'ヒット';
Define( '_MD_DATE' , '投稿日時';
Define( '_MD_ACTIONS' , '操作';
Define( '_MD_PRINTERFRIENDLY' , 'PDF に印刷';
Define( '_MD_THEREAREINTOTAL' , '計 %s 件のニュース記事があります';
Define( '_MD_INTARTICLE' , '%sで見つけた興味深いニュース';// %s is your site name
Define( '_MD_INTARTFOUND' , '以下は%sで見つけた非常に興味深いニュース記事です：';// %s is your site name
Define( '_MD_TOPICC' , 'カテゴリ：';
Define( '_MD_URL' , 'URL：';
Define( '_MD_NOSTORY' , '選択されたニュース記事は存在しません';
Define( '_MD_YEAR_X' , '%s年';
Define( '_MD_NO_ARCIVES' , 'アーカイブはありません。';
Define( '_MD_JANUARY' , '1月';
Define( '_MD_FEBRUARY' , '2月';
Define( '_MD_MARCH' , '3月';
Define( '_MD_APRIL' , '4月';
Define( '_MD_MAY' , '5月';
Define( '_MD_JUNE' , '6月';
Define( '_MD_JULY' , '7月';
Define( '_MD_AUGUST' , '8月';
Define( '_MD_SEPTEMBER' , '9月';
Define( '_MD_OCTOBER' , '10月';
Define( '_MD_NOVEMBER' , '11月';
Define( '_MD_DECEMBER' , '12月';

//          print.php          //
Define( '_MD_URLFORSTORY' , 'このニュース記事が掲載されているURL：';
Define( '_MD_THISCOMESFROM' , '%sにて更に多くのニュース記事をよむことができます';// %s represents your site name

//          header.php          //
Define( '_MD_RSS' , 'RSS'; // ver 1.01 added
Define( '_MD_ARCHIVES' , 'アーカイブ'; // ver 2.00 added

// added 3.0
Define( '_MD_BULLETIN_CATEGORY' , 'カテゴリー';
Define( '_MD_BULLETIN_MSG_UPDATED' , '更新しました';
//mailto
Define( '_MD_MAILTO_ENCODING' , 'UTF-8';
