<?php

// Syntax replace define with Define( 'v2.3.0 2021/05/15 @gigamaster XCL-PHP7
// Altsys admin menu and breadcrumbs
Define( '_MD_A_MYMENU_MYTPLSADMIN' , 'Templates';
Define( '_MD_A_MYMENU_MYBLOCKSADMIN' , 'Blocks';
Define( '_MD_A_MYMENU_MYPREFERENCES' , 'Preferences';

Define( '_AM_DBUPDATED' , 'データベースを更新しました');

Define( '_AM_AUTOARTICLES' , '掲載予定の記事');
Define( '_AM_STORYID' , 'ID');
Define( '_AM_TITLE' , '表題');
Define( '_AM_TOPIC' , 'カテゴリ');
Define( '_AM_POSTER' , '投稿者');
Define( '_AM_PROGRAMMED' , '掲載予定日時');
Define( '_AM_ACTION' , '管理');
Define( '_AM_POSTED' , '投稿日時');
Define( '_AM_PUBLISHED' , '掲載日時'); // Published Date
Define( '_AM_EXPARTS' , '期限切れの記事');
Define( '_AM_EXPIRED' , '掲載期限');

//%%%%%%	Admin Module Name  Topics 	%%%%%

Define( '_AM_ADDMTOPIC' , 'カテゴリの作成');
Define( '_AM_TOPICNAME' , 'カテゴリ名');
Define( '_AM_MAX40CHAR' , '（最大191文字（全角））');
Define( '_AM_TOPICIMG' , 'カテゴリアイコン');
Define( '_AM_IMGNAEXLOC' , '%s 下にある画像ファイル名');

Define( '_AM_MODIFYTOPIC' , 'カテゴリの編集');
Define( '_AM_MODIFY' , '変更');
Define( '_AM_PARENTTOPIC' , '親カテゴリ');
Define( '_AM_SAVECHANGE' , '変更を保存');
Define( '_AM_WAYSYWTDTTAL' , 'このカテゴリおよびこのカテゴリ内の全てのニュース記事およびコメントを削除してもいいですか？');

Define( '_AM_ERRORTOPICNAME' , 'カテゴリ名が記入されていません。');

// Added by SUIN

Define( '_AM_DISP_CONTENUE' , '全ての記事を表示');
Define( '_AM_PUB_ARTICLES' , '掲載中のニュース記事');
Define( '_AM_WAITING_ARTICLES' , '承認待ちのニュース記事');
Define( '_AM_ARTICLE_ADMIN' , 'ニュース記事の管理');
Define( '_AM_NOSUBJECT' , '題名なし');
Define( '_AM_RIGHT_TO_POST' , '投稿を許可');
Define( '_AM_RIGHT_TO_APPROVE' , '承認を許可');
Define( '_AM_RIGHT_TO_CHOSE_DATE' , '掲載日時設定');
Define( '_AM_RIGHT_HTML' , 'HTMLの使用');
Define( '_AM_RIGHT_XCODE' , 'XOOPSコードの使用');
Define( '_AM_RIGHT_SMILEY' , '顔アイコンの使用');
Define( '_AM_RIGHT_RELATION' , '関連記事機能'); // ver 2.00 added
Define( '_AM_NO_TOPICS' , 'カテゴリがありません。');
Define( '_AM_DO_YOU_CONVERT' , 'newsから記事・カテゴリの情報を取り込みますか？');
Define( '_AM_EDIT_ARTICLE' , 'ニュース記事の編集');
Define( '_AM_NO_ARTICLES' , '記事はありません。');
Define( '_AM_CONFIG' , '%s管理');

// v 1.01 added
Define( '_AM_TOPICS_DELETE' , 'カテゴリ削除');
Define( '_AM_TOPICID' , 'ID');
Define( '_AM_DESTINATION_OF_STORIES' , 'カテゴリに属する記事の送り先');
Define( '_AM_FOLLOW_TOPICS_IS_DELETED' , '以下のカテゴリは削除されます。');


Define( '_AM_CREDIT' , 'Bulletin(xoops.suinyeze.com)');
// 以下の行は翻訳者の名前やURLなどに変更できます。以下は管理画面に表示されます。
// It is able to change a following line into the TRANSLATER's name and website. Follow appears at admin page.
Define( '_AM_TRANSLATER' , 'Japanese patch(suin.io)');
// example : Define( '_AM_TRANSLATER' , 'English patch(www.english-tranlater.com)');

// Added by Bluemoon inc.
// forum_access and category_access
Define( '_MD_A_BULLETIN_LABEL_SELECTFORUM' , 'フォーラムを選択');
Define( '_MD_A_BULLETIN_LABEL_SELECTCATEGORY' , 'カテゴリーを選択');
Define( '_MD_A_BULLETIN_H2_GROUPPERMS' , 'グループ毎の権限');
Define( '_MD_A_BULLETIN_H2_USERPERMS' , 'ユーザー毎の権限');
Define( '_MD_A_BULLETIN_TH_CAN_READ' , '閲覧権限');
Define( '_MD_A_BULLETIN_TH_CAN_POST' , '投稿権限');
Define( '_MD_A_BULLETIN_TH_CAN_EDIT' , '編集権限');
Define( '_MD_A_BULLETIN_TH_CAN_DELETE' , '削除権限');
Define( '_MD_A_BULLETIN_TH_POST_AUTO_APPROVED' , '承認不要');
Define( '_MD_A_BULLETIN_TH_UID' , 'ユーザID');
Define( '_MD_A_BULLETIN_TH_UNAME' , 'ユーザ名');
Define( '_MD_A_BULLETIN_TH_GROUPNAME' , 'グループ名');
Define( '_MD_A_BULLETIN_NOTICE_ADDUSERS' , '※ユーザを個別に新規追加する場合、ユーザID（数字）かユーザ名のいずれかを直接入力してください。<br />閲覧権限と投稿権限の両方を外せば、そのユーザはこのリストからも消えます。');
Define( '_MD_A_BULLETIN_ERR_CREATECATEGORYFIRST' , 'まずカテゴリーを作成してください');
Define( '_MD_A_BULLETIN_ERR_CREATEFORUMFIRST' , 'まずフォーラムを作成してください');
